#!/bin/bash
##=====================================================================##
# Script to add / remove links from the startpage.html file for badwolf #
## =========================== version 0.2 =========================== ##

### Get input
PROGRAM_INPUT="${@}"

STARTPAGE="$HOME/.config/badwolf/startpage.html"
### Create temp files
STARTPAGE_TEMP=$(mktemp)
FAVORITES_TEMP=$(mktemp)
YAD_FAVORITE_LIST=$(mktemp)
SELECTED_STEP=$(mktemp)
LOAD_URL=$(mktemp)
### Initial program values
echo "main" > $SELECTED_STEP
echo "exit" > $LOAD_URL

check_problems(){
	### If yad doesn't exist exit
	if [ ! -x /usr/bin/yad ]; then
		#echo $"Error: yad is not installed or cannot be found. Exiting script."
		exit 1
	fi

	### Check if startpage exists
	if [ ! -f "$STARTPAGE" ]; then
		error_dialog $"Not found" $"startpage.html was not found \
in expected folder.\nCannot check favorites"
		#echo $"$STARTPAGE cannot be found. Exiting script."
		exit 1
	fi

	### Check if startpage.html contains a place to insert favourites
	if [ $(cat "$STARTPAGE" | grep -c "<!--FAVS-start-->") -eq 1 ] \
	&& [ $(cat "$STARTPAGE" | grep -c "<!--FAVS-end-->") -eq 1 ]; then
		### Load startpage to temp file
		cat "$STARTPAGE" > $STARTPAGE_TEMP
		
		### Read startpage.html and save all favorite links to FAVORITES_TEMP
		COPY_LINE=0
		while read -r line; do
			### Don't save any more lines and exit while loop
			if [[ "$line" == "<!--FAVS-end-->" ]] ; then break; fi
			### Save line if it contains 2 elements ("<a href=" and ">*</a><br>$") only once.
			if [ $COPY_LINE -eq 1 ] && [ $(echo "$line" | grep -c "<a href=") -eq 1 ] && [ $(echo "$line" | grep -c ">*</a><br>") -eq 1 ]; then
				echo "$line" >> $FAVORITES_TEMP
			fi
			### Next line, save content
			if [[ "$line" == "<!--FAVS-start-->" ]]; then COPY_LINE=1; fi
		done < "$STARTPAGE_TEMP"
	else
		error_dialog $"Contains errors" $"startpage.html isn't prepared for storing favorites."
		#echo $"$STARTPAGE isn't prepared for storing favorites. Exiting script."
		exit 1
	fi
}

#### Confirmation Dialog ####
confirm_dialog(){
	local YAD_TITLE="${1}"
	local YAD_TEXT="${2}"
	
	yad --class="badwolf-favorites" --name="Confirmation-Check" \
	--window-icon=badwolf --title="$YAD_TITLE" --borders=20 --wrap \
	--text="$YAD_TEXT" --text-align=center --center --width=300 --height=150 \
	--button=$"gtk-yes":0 --button=$"gtk-cancel":1 --buttons-layout=center
	local exitcode=$?
	if [ $exitcode -eq 0 ]; then return 0;
	else return 1; fi
}

#### Display an error dialog ####
error_dialog(){
	local ERR_TITLE="${1}"
	local ERR_TEXT="${2}"
	
	yad --image="gtk-dialog-error" --borders=10 --center \
		--title="$ERR_TITLE" --class="badwolf-favorites" --name="Error-Window" \
		--form --align=center --separator="" \
		--field="$ERR_TEXT":LBL '' \
		--window-icon=gtk-dialog-error --button=gtk-close:1
}

#### Save the step that has been selected ####
save_step(){
	local STEP="${@}"
	echo "$STEP" > $SELECTED_STEP
}

#### Read Favorites and prepare file for yad ####
read_favorites(){
	### empty YAD_FAVORITE_LIST
	> $YAD_FAVORITE_LIST
	### If Favorites were found, process links
	if [[ -s $FAVORITES_TEMP ]]; then
		LINK_COUNT=1
		while read -r line; do
			FAVORITE_LINK="$(echo "$line" | cut -d '"' -f2)"
			FAVORITE_NAME="$(echo "$line" | cut -d '>' -f2-)"
			FAVORITE_NAME="${FAVORITE_NAME%%</a><br>*}"
			echo $LINK_COUNT >> $YAD_FAVORITE_LIST
			echo $FAVORITE_NAME >> $YAD_FAVORITE_LIST
			echo $FAVORITE_LINK >> $YAD_FAVORITE_LIST
			LINK_COUNT=$((++LINK_COUNT))
		done < "$FAVORITES_TEMP"
	fi
}

#### Save all favorite links back to startpage.html ####
save_favorites(){
	local START_LINE
	local END_LINE
	### Make sure the user really wants to save the changes
	if confirm_dialog $"Save changes" $"Your favorite links will be replaced with your changes. Is this ok?"; then
		# Remove links from startpage.html (TEMP)
		START_LINE=$(cat $STARTPAGE_TEMP | grep -n -m1 "<!--FAVS-start-->" | cut -d":" -f1)
		START_LINE=$((++START_LINE))
		END_LINE=$(cat $STARTPAGE_TEMP | grep -n -m1 "<!--FAVS-end-->" | cut -d":" -f1)
		END_LINE=$((--END_LINE))
		
		# Only remove favorites if non there
		if [ $END_LINE -ge $START_LINE ]; then
			sed -i "${START_LINE},${END_LINE}d" $STARTPAGE_TEMP
		fi
		# Save links to startpage.html (temp)
		sed -i "$((--START_LINE)) r $FAVORITES_TEMP" $STARTPAGE_TEMP
		# Replace real startpage.html
		cat "$STARTPAGE_TEMP" > $STARTPAGE
	fi
	
	### Set program to exit
	save_step "exit"
}

add_link(){
	#local SELECTED_FAVORITE="${*}"
	local POSITION
	local LINK_URL
	local LINK_NAME
	local TOTAL_LINKS
	
	### Triming spaces at the end
	#SELECTED_FAVORITE="$(echo $SELECTED_FAVORITE |  sed -e 's/[[:space:]]*$//')"
	### Geting each value
	#LINK_URL="$(echo $SELECTED_FAVORITE | rev | cut -d" " -f1 | rev)"
	#LINK_NAME="$(echo $SELECTED_FAVORITE | cut -d" " -f2-)"
	#LINK_NAME="${LINK_NAME% *}"
	
	TOTAL_LINKS=$(wc -l < $FAVORITES_TEMP | awk '{ print $1 }')
	POSITION=$((++TOTAL_LINKS))
	
	yad --class="badwolf-favorites" --name="Add-Link" \
	--window-icon=badwolf --title=$"Add link" --borders=20 \
	--text=$"Give information for a new link" \
	--text-align=center --center --form --focus-field=1 \
	--field=$"Name":CBE "$LINK_NAME" --field=$"URL":CBE "$LINK_URL" \
	--field=$"Position":NUM "$POSITION[!1..$TOTAL_LINKS[!1]]" \
	--field="":LBL '' \
	--field=$"gtk-add!Add link!Save new link":FBTN 'bash -c "save_link %3 %2 %1; kill -USR2 $YAD_PID"' \
	--width=500 --no-buttons
}

edit_link(){
	local SELECTED_FAVORITE="${@}"
	local POSITION
	local LINK_URL
	local LINK_NAME
	local TOTAL_LINKS
	
	### Triming spaces at the end
	SELECTED_FAVORITE="$(echo $SELECTED_FAVORITE |  sed -e 's/[[:space:]]*$//')"
	### Geting each value
	POSITION="$(echo $SELECTED_FAVORITE | cut -d" " -f1)"
	export POSITION
	LINK_URL="$(echo $SELECTED_FAVORITE | rev | cut -d" " -f1 | rev)"
	LINK_NAME="$(echo $SELECTED_FAVORITE | cut -d" " -f2-)"
	LINK_NAME="${LINK_NAME% *}"
	TOTAL_LINKS=$(wc -l < $FAVORITES_TEMP | awk '{ print $1 }')
	
	yad --class="badwolf-favorites" --name="Edit-Link" \
	--window-icon=badwolf --title=$"Edit link" --borders=20 \
	--text=$"Edit the information of the link:" \
	--text-align=center --center --form --focus-field=1 \
	--field=$"Name":CBE "$LINK_NAME" --field=$"URL":CBE "$LINK_URL" \
	--field=$"Position":NUM "${POSITION}[!1..${TOTAL_LINKS}[!1]]" \
	--field="":LBL '' \
	--field=$"gtk-apply!!Save changes":FBTN 'bash -c "save_link %3 %2 %1 $POSITION; kill -USR2 $YAD_PID"' \
	--field=$"gtk-remove!!Remove link":FBTN 'bash -c "save_link \"remove\" %2 %1 $POSITION; kill -USR2 $YAD_PID"' \
	--width=500 --no-buttons
}

#### Edit FAVORITES temp file ####
save_link(){
	local NEW_POSITION="${1}"
	local LINK_URL="${2}"
	local LINK_NAME="${3}"
	local OLD_POSITION="${4}"
	local URL_TEXT
	
	# LINK_NAME must NOT be empty
	if [[ "$(echo "$LINK_NAME" | sed 's/ //g')" == "" ]]; then
		error_dialog $"No link name" $"Link name is empty. Link will not be saved."
		return 1
	# URL must not be empty
	elif [[ "$(echo "$LINK_URL" | sed 's/ //g')" == "" ]]; then
		error_dialog $"No URL" $"URL is empty. Link will not be saved."
		return 1
	fi
	
	# Make sure the user wants to remove the link
	if [[ "$NEW_POSITION" == "remove" ]]; then
		if ! confirm_dialog $"Remove link" $"The link for $LINK_NAME will be removed. Is this ok?"; then
			return 1
		fi
	fi
	
	# Creating link string
	URL_TEXT="<a href=\"${LINK_URL}\">${LINK_NAME}</a><br>"
	
	# Remove old if it exists
	if [ ! -z "$OLD_POSITION" ]; then
		sed -i "${OLD_POSITION}d" $FAVORITES_TEMP
	fi
	
	# Total number of remaining links in favorites temp
	local TOTAL_LINKS=$(wc -l < $FAVORITES_TEMP | awk '{ print $1 }')
	
	### Adding link to Favorites file if NEW_POSITION is a number
	if [[ $NEW_POSITION == ?(-)+([0-9]) ]]; then
		# Number smaller than 1, use 1
		if [ $NEW_POSITION -lt 1 ]; then NEW_POSITION=1; fi
		# No links saved yet, use echo >
		if [ $TOTAL_LINKS -eq 0 ]; then
			echo "$URL_TEXT" > $FAVORITES_TEMP
		# Number greater than total links , save under last
		elif [ $NEW_POSITION -gt $TOTAL_LINKS ]; then
			sed -i "$ a $URL_TEXT" $FAVORITES_TEMP
		# Any other number (between 1 and TOTAL_LINKS
		else
			sed -i "${NEW_POSITION} i $URL_TEXT" $FAVORITES_TEMP
		fi
	fi
}

#### manage favorites dialog ####
main_dialog(){
	### Ready the script to exit
	echo "exit" > $SELECTED_STEP
	
	### Read favorites file
	read_favorites
	
	# Backup links
	cat $FAVORITES_TEMP > "$HOME/.config/badwolf/favorites.backup"
	
	# Some day maybe I can send the output to badwolf, but C is too much for me.
	#~ SELECTED_FAVORITE=$(yad --class="badwolf-favorites" --name="Manage-Favorites" \
	#~ --window-icon=badwolf --title=$"Manage favorites" --borders=20 \
	#~ --text=$"Add, remove and save your favorite links to startpage.html" \
	#~ --text-align=center --center --separator=" " --list  --search-column=2 \
	#~ --column="#" --column=$"Favorite Name" --column=$"Favorite URL" --width=700 --height=400 \
	#~ --button=$"Launch!gtk-goto-top!Open link in badwolf":0 \
	#~ --button=$"Edit Link!gtk-remove!Edit link information or remove it from favorites":'bash -c "save_step edit_favorite; kill -USR1 $YAD_PID"' \
	#~ --button=$"Add Link!gtk-add!Add new link to favorites":'bash -c "save_step add_favorite; kill -USR2 $YAD_PID"' \
	#~ --button=$"Save!gtk-save!Save list of links to favorites":'bash -c "save_favorites"' < $YAD_FAVORITE_LIST)
	
	### Display Favorites in yad window
	SELECTED_FAVORITE=$(yad --class="badwolf-favorites" --name="Manage-Favorites" \
	--window-icon=badwolf --title=$"Manage favorites" --borders=20 \
	--text=$"Add, remove and save your favorite links to startpage.html" \
	--text-align=center --center --separator=" " --list  --search-column=2 \
	--column="#" --column=$"Favorite Name" --column=$"Favorite URL" --width=700 --height=400 \
	--button=$"Edit Link!gtk-remove!Edit link information or remove it from favorites":'bash -c "save_step edit_favorite; kill -USR1 $YAD_PID"' \
	--button=$"Add Link!gtk-add!Add new link to favorites":'bash -c "save_step add_favorite; kill -USR2 $YAD_PID"' \
	--button=$"Save!gtk-save!Save list of links to favorites":'bash -c "save_favorites"' < $YAD_FAVORITE_LIST)

	### See what selection was made
	local STEP="$(cat $SELECTED_STEP)"
	#echo "Step: $STEP, Selection: $SELECTED_FAVORITE"
	### Close the favorite manager window
	if [ -z "$SELECTED_FAVORITE" ] && [[ "$STEP" == "exit" ]]; then
		exit 1
	### Add new Link to favorites
	elif [ -z "$SELECTED_FAVORITE" ] && [[ "$STEP" == "add_favorite" ]]; then
		add_link
	### Edit selected link's information
	elif [ ! -z "$SELECTED_FAVORITE" ] && [[ "$STEP" == "edit_favorite" ]]; then
		edit_link "$SELECTED_FAVORITE"
	### Double click will edit the bookmark.
	# Someday someone will make double click send the link to badwolf so it open on a new tab
	elif [ ! -z "$SELECTED_FAVORITE" ]; then
		save_step "edit_favorite"
		edit_link "$SELECTED_FAVORITE"
	fi
	#~ ### Launch selected link in badwolf browser
	#~ elif [ ! -z "$SELECTED_FAVORITE" ]; then
		#~ SELECTED_FAVORITE="$(echo $SELECTED_FAVORITE |  sed -e 's/[[:space:]]*$//')"
		#~ LINK_URL="$(echo $SELECTED_FAVORITE | rev | cut -d" " -f1 | rev)"
		#~ echo $LINK_URL > $LOAD_URL
	#~ fi
}


### Remove temp files
cleanup() {
	local PROGRAM_OUTPUT="$(cat $LOAD_URL)"
    ### Remove temporary files
    rm -f -- "$STARTPAGE_TEMP"
    rm -f -- "$FAVORITES_TEMP"
    rm -f -- "$YAD_FAVORITE_LIST"
    rm -f -- "$SELECTED_STEP"
    rm -f -- "$LOAD_URL"
	echo "$PROGRAM_OUTPUT"
}

### Set trap on EXIT for cleanup
trap cleanup EXIT

#### Export functions ####
export -f check_problems
export -f save_step
export -f read_favorites
export -f save_favorites
export -f add_link
export -f edit_link
export -f save_link
export -f error_dialog
export -f confirm_dialog
export -f main_dialog

#### Export variables ####
export PROGRAM_INPUT
export STARTPAGE
export STARTPAGE_TEMP
export FAVORITES_TEMP
export YAD_FAVORITE_LIST
export SELECTED_STEP
export LOAD_URL

# First check for problems
check_problems

#~ #Check if incomming URL for saving.
#~ #For some other day when I can send the URL and tab title to add_link
#~ while [[ $# -ge 1 ]]; do
	#~ case $1 in
		#~ url=*) add_link "$PROGRAM_INPUT";;
	#~ esac
	#~ shift
#~ done

# Main dialog for adding, removing, saving and launching links
while [[ "$(cat $SELECTED_STEP)" != "exit" ]]; do
	main_dialog
done
